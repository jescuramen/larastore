<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function user()
    {
      return $this->belongsTo('\App\User');
    }

    //define the many to many relationships between the Order model and the Product model
    //specifying the name of the pivot table product_orders and that other than the foreign keys of
    //the two models, this table also has columns named quantity and subtotal
    public function products()
    {
      return $this->belongsToMany('App\Product', 'products_orders')
      ->withPivot('quantity', 'subtotal')
      ->withTimestamps();
    }
}
