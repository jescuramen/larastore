<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //define a gate that will check if an authenticated user has an admin role
        //defining a gate requires 2 parameters
          //name of gate
          //anonymous function that will return a boolean value
        Gate::define('isAdmin', function($user){
          return $user->isAdmin === 1;
        });
    }
}
