<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
//import class Auth
use Auth;
//import Product model
use App\Product;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(Auth::user());

        //if an admin user is going to access this view, show all transactions
        if(Auth::user()->isAdmin === 1){
          $orders = Order::all();
        }else{
          //if a non-admin user is to access this view, show only the transactions owned by the authenticated user
            //use the where() method of the Order model to specify what column name has to match what value
              //1st argument is the column name
              //2nd argument is the value to be matched
            //it needs a get method
          $orders = Order::where('user_id', Auth::user()->id)->get();
        }
        //dd($orders);
        return view('orders')->with('orders', $orders);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order;
        //set the user_id  foreign key of the order object as the authenticated
        //user's id via the user() static method of the Auth class
        $order->user_id = Auth::user()->id;
        //set the total amount of this order by getting the request input named total sent from
        //the JS fetch request
        $order->total = $request->input('total');
        //save this new order object in our orders table via the save() method
        $order->save();

        //fill in the  quantities and subtoal in the products_orders pivot table
        //based on the session cart contents
        foreach ($request->session()->get('cart') as $id => $quantity){
          //get the product details
          $product = Product::find($id);
          //compute for the subtotal
          $subtotal = $product->price * $quantity;
          //use the attach() helper method to fill in the custom columns in the products_orders pivot table
          $order->products()->attach($id, [
            //if multiple custom columns are to be given values, use an associative array as the second
            //argument to the attach() method.
              //key == custom column name
              //value == the value to be recorded in that custom column
            'quantity' => $quantity,
            'subtotal' => $subtotal
          ]);
        }

        //we can now clear the cart session variable
        $request->session()->forget('cart');

        //generate a flash session variable containing a status message
        $request->session()->flash('status', "Thank you for shopping! Your order has been finalized.");

        //no need to return redirect because the request happens with JS fetch
        //the redirection should happen with Javascript paypal.js
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
