<?php

//this controller is a conventional controller and not a resource controller
//php artisan make:controller CartController

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//import Product model
use App\Product;
//import Session
use Session;

class CartController extends Controller
{
    public function index()
    {
      //initialize an empty array where products from the cart can be pushed into
      $product_cart = [];
      if(Session::has('cart')){
        $cart = Session::get('cart');
        $total = 0;

        //iterate over each key-value pair within the $cart session variable naming the key as $id
        //and the value as $quantity
        foreach($cart as $id => $quantity){
          //query the products inside the cart
          $product = Product::find($id);
          // declare new properties that is not in the database products table
            //therefore we can't save it straight forward.
            //we need to keep the key-value pair in a different way
          //declare a quantity and subtotal property for later use
          $product->quantity = $quantity;
          $product->subtotal = $product->price * $quantity;
          //increment the total with the subtotal for every iteration
          $total += $product->subtotal;
          //for every iteration, "push this $product into the $product_cart array"
          //the below code is equivalent to $product_cart.array_push($product)
          $product_cart[] = $product;
        }
        //since the data is an array we can't use with() rather use compact()
        //this will send the data to the view
        return view('cart', compact('product_cart', 'total'));
      };
      //if no session cart the data sent to view is just an empty array of product_cart
      return view('cart', compact('product_cart'));
    }

    public function store(Request $request)
    {
      //validate the $request sent by the form
      $request->validate([
        'id' => 'required',
        'quantity' => 'required'
      ]);
      //dd($request->input('id').": ".$request->input('quantity'));
      $id = $request->input('id');
      $quantity = $request->input('quantity');

      //check if we have a session variable named cart
      if($request->session()->has('cart')){
        //if we do, get it and save it as a variable named $cart
        $cart = $request->session()->get('cart');
      }else{
        //else, initialize $cart as an empty array
        $cart = [];
      }

      //search the $cart variable if it has a key matching the submitted product id
      if(isset($cart[$id])){
        //if yes, increment with the newly submitted product quantity
        $cart[$id] += $request->input('quantity');
      }else{
        //else, set as a new key within the associative array $cart
        $cart[$id] = $request->input('quantity');
      }

      //put the $cart-(array) variable as a session variable named cart
      $request->session()->put('cart', $cart);

      //query for the product information so that we can flash a notification message
      //stating that a given quantity of this product name has been added to the cart
      $product = Product::find($id); //make sure to import the Product model first

      //set the flash variable containing the notification message
      $request->session()->flash('status', $quantity." quantity(s) of ". $product->name ." has/have been added to cart.");

      //redirect back to catalogue
      return redirect('/products');
    }

    //we need $id and $request->input(newQty) as parameters for this action
    public function update($id, Request $request)
    {
      //set validation rule to avoid a null quantity submission
      $request->validate([
        //when validating, use key from form input name from the cart.blade.php
        'newQty' => 'required'
      ]);

      //if validation succeeds, get the new quantity from the request input and store
      //as a variable named $newQty
      $newQty = $request->input('newQty');

      //retrieve the session cart variable and save it as a variable named $cart
      $cart = $request->session()->get('cart');

      //set the newly received quantity from the cart update form
      //as the current quantity of this product id in the $cart variable
      $cart[$id] = $newQty;

      //put the $cart variable as a session variable named cart,
      //effectively overwriting the previous quantity
      $request->session()->put('cart', $cart);

      //query for the product info for displaying a dynamically generated
      //notification message using a session flash variable
      $product = Product::find($id);

      //set the session flash variable with the corresponding message
      $request->session()->flash('status', $product->name . "'s quantity has been updated to " . $newQty . ".");

      //redirect back to catalogue
      return redirect('/cart');
    }

    public function destroy($id, Request $request)
    {
      //diedump to visualize which key is needed for the forget()
      //dd($request->session()->get("cart.$id"));

      //remove the specific product from our cart session variable using the forget() method
      $request->session()->forget("cart.$id");

      $product = Product::find($id);

      $request->session()->flash('status', $product->name . " has been removed from cart.");

      return redirect('/cart');
    }

    public function clearCart(Request $request)
    {
      $request->session()->flush();
    }
}
