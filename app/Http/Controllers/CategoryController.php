<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //implement authorization via Laravel Policy
        $this->authorize('create', Category::class);
        //category came from addCat.js
        //retrieve the property named category from the FormData object sent via JS Fetch and save it as a variable named $name
        $name = $request->input('category');

        //validate if category name was received
        if(isset($name)){
          //check if data type is correct and is not empty
          if((gettype($name) === "string") && ($name != "")){
            //selectively query for a category whose name field matches our received $name value.
            //duplicate checking of the input vs the categories table in db and only get the 1st one
              //'name' from categories table
              //$name is the variable above.
            $duplicate = Category::where(strtolower('name'), strtolower($name))->first();
            //if no duplicates found
            if($duplicate === null){
              //sanitize our input
              $cleanName = htmlspecialchars($name);
              //instantiate a new category object from the Category model
              $category = new Category;
              //set the value of this category's name to be the sanitized form input
              $category->name = $cleanName;

              //if successfully saved
              if($category->save()){
                return response()->json([
                  'message' => "Category added successfully.",
                  //the data property of our response contains the HTML element that we want to append
                  //to our <select> element in the products.create view
                  'data' => "<option value=\"$category->id\">$category->name</option>"
                ], 201); //successfull creation of a resource is denoted by an HTTP status code of 201
              }else{//failed to savethe new $category
                return response()->json([
                  'message' => "Failed to save new category."
                ], 500);
              }
            }else{//duplicate found
              return response()->json([
                'message' => "Category already exists."
              ], 403);
            }
          }else{//submitted name is either not a string or an empty string
            return response()->json([
              'message' => "Category name is required and it has to be a string."
            ], 403);//403 stands for forbidden action, this is different from 401 which is unathorized

          }
        }else{//no category name was received from JS fetch in the create.blade.php view
          return response()->json([
            'message' => "No category name was received."
          ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }
}
