<?php

namespace App\Http\Controllers;

use App\Product;
//import the class Category
use App\Category;
use Illuminate\Http\Request;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //test: return "Congratulations you have added a new product.";
      //query all of our products
      $products = Product::all();
      return view('products.index')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //use the authorize() controller helper to check the create() action of the ProductPolicy class
        //if authorization fails the check, we expect an HTTP response of status code 403
        $this->authorize('create', Product::class);
        $categories = Category::all();
        return view('products.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Product::class);

        //call the validate() method on our request to apply validation
        //rules on specified request input
        $request->validate([
          //from form input names in add product card of create.blade.php
          'name' => 'required|string|unique:products,name',
          'description' => 'required|string',
          'price' => 'required',
          'category' => 'required',
          'image' => 'required|image'
        ]);

        //sanitize the inputs
        $name = htmlspecialchars($request->input('name'));
        $description = htmlspecialchars($request->input('description'));
        $price = htmlspecialchars($request->input('price'));
        $category = htmlspecialchars($request->input('category'));
        $image = $request->file('image');

        //instantiate the product object from the Product model named $product
        $product = new Product;
        //set the properties of this object as defined in its migration to their respective
        //sanitized form input values
        $product->name = $name;
        $product->description = $description;
        $product->price = $price;
        $product->category_id = $category;
        //handle the file image upload
        //set the filename of the uploaded image to be the time of upload retaining the original file type
        $file_name = time() . "." . $image->getClientOriginalExtension();
        //set target destination where the file will be saved in
        $destination = "images/";
        //call the move() method of the image object to save the uploaded file in the target destination
        //under the specified file name
        $image->move($destination, $file_name);
        //set the path of the saved image as the value for the column img_path of this record
        $product->img_path = $destination . $file_name;

        //save the new product object as a new record in the products table via its save() method
        $product->save();

        //this will now call the index action of the ProductController as per Laravel's resourceful routes
        //redirect is a get request
        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //die-dump the name of the product to be edited
        //this works due to route-model binding
          //dd($product->name);

        //use the authorize() controller helper to call the update method in the ProductPolicy
        $this->authorize('update', Product::class);
        $categories = Category::all();

        //we can view to products.edit with 2 parameters with it
          //$categories from the category query
          //$product from the model route binding
        return view('products.edit')->with('categories', $categories)->with('product', $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //die-dump both parameter $request and $product to check if we are able to get the value
        //dd($request->input('name'));
        //dd($product->name);
        $this->authorize('update', Product::class);

        $request->validate([
          //from form input names in add product card of create.blade.php
          'name' => 'required|string',
          'description' => 'required|string',
          'price' => 'required',
          'category' => 'required',
          'image' => 'image'
        ]);

        //sanitize the inputs
        $name = htmlspecialchars($request->input('name'));
        $description = htmlspecialchars($request->input('description'));
        $price = htmlspecialchars($request->input('price'));
        $category = htmlspecialchars($request->input('category'));
        //No need for image here
        //$image = $request->file('image')

        //$product is the product object to be edited, this was obtained via Laravel's
        //route-model binding
        //overwrite the properties of $product with the input values from the edit form
        $product->name = $name;
        $product->description = $description;
        $product->price = $price;
        $product->category_id = $category;

        //if an image file upload is found, replace the current image of the product with the new upload
        if($request->file('image') != null){
          //handle the file image upload
          //set the filename of the uploaded image to be the time of upload retaining the original file type
          $file_name = time() . "." . $image->getClientOriginalExtension();
          //set target destination where the file will be saved in
          $destination = "images/";
          //call the move() method of the image object to save the uploaded file in the target destination
          //under the specified file name
          $image->move($destination, $file_name);
          //set the path of the saved image as the value for the column img_path of this record
          $product->img_path = $destination . $file_name;
        }
        $product->save();

        //redirect
        return redirect("/products/" . $product->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if($product->isActive == 1){
          //disable the product in question
          $product->isActive = 0;
        }else{
          //reactivate the product in question
          $product->isActive = 1;
        }
        //save it
        $product->save();

        return redirect('/products/' . $product->id);
    }
}
