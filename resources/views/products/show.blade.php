@extends('layouts.app')

@section('content')
  <div class="col-8 offset-2">
    <div class="card">
      <img src="{{asset($product->img_path)}}" class="card-img-top img-fluid">

      <div class="card-body">
        <h4 class="card-title">{{$product->name}}</h4>
        <h5>Price: {{$product->price}}</h5>
        @can('isAdmin')
          <h5>
            Status:
            @if($product->isActive == 1)
              Active
            @else
              Inactive
            @endif
          </h5>
        @endcan
        <p class="card-text">{{$product->description}}</p>
        {{-- button leading back to admin dashboard / catalogue --}}
        <a class="btn btn-info mb-1" href="/products/">Products</a>
        @can('isAdmin')
          <a class="btn btn-warning mb-1" href="/products/{{$product->id}}/edit">Edit</a>
          <form method="POST" action="/products/{{$product->id}}">
            @csrf
            @method('DELETE')
            @if($product->isActive == 1)
              <button type="submit" class="btn btn-danger">Deactivate</button>
            @else
              <button type="submit" class="btn btn-success">Reactivate</button>
            @endif
          </form>
        @endcan
      </div>
    </div>
  </div>
@endsection
