@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-8 offset-2">
      <div id="accordion">
        {{-- start of add category card --}}
        <div class="card">
          <div class="card-header">
            <a class="card-link h3" data-toggle="collapse" href="#addCategory">
              Add Category
            </a>
          </div>

          <div id="addCategory" class="collapse" data-parent="#accordion">
            <div class="card-body">
              <div id="addCatNotif">
                {{-- to be filled this empty DIV --}}
              </div>
              {{-- this form will use fetch api, no method and action --}}
              <form>
                <div class="form-group">
                  <label for="name">Category name: </label>
                  <input class="form-control" type="text" id="catName">
                </div>
              </form>
              {{-- button is outside the form --}}
              <button class="btn btn-success" id="addCatBtn">Add Category</button>
            </div>
          </div>
        </div>
        {{-- end of Add category card --}}

        {{-- start of Add product card --}}
        <div class="card">
          <div class="card-header">
            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif
            <a class="card-link h3" data-toggle="collapse" href="#addProduct">
              Add Product
            </a>
          </div>
          <div id="addProduct" class="collapse" data-parent="#accordion">
            <div class="card-body">
              <form class="" action="/products" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <label for="name">Name: </label>
                  <input class="form-control" type="text" name="name" id="name">
                </div>
                <div class="form-group">
                  <label for="description">Description: </label>
                  <input class="form-control" type="text" name="description" id="description">
                </div>
                <div class="form-group">
                  <label for="price">Price: </label>
                  <input class="form-control" type="number" name="price" id="price">
                </div>
                <div class="form-group">
                  <label for="category_id">Category: </label>
                  <select class="form-control" id="category_id" name="category">
                    <option>Select a category:</option>
                    @if(count($categories) > 0)
                      @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                      @endforeach
                    @endif
                  </select>
                </div>
                <div class="form-group">
                  <label for="image">Image:</label>
                  <input class="form-control" type="file" name="image" id="image">
                </div>
                <button type="submit" class="btn btn-success">Add Product</button>
              </form>
            </div>
          </div>
        </div>
        {{-- end of add product card --}}
      </div>
      {{-- end of accordion --}}
    </div>
  </div>
  {{-- asset is a namespace and will look in the public folder --}}
  <script src="{{asset('js/addCat.js')}}">

  </script>

@endsection
