@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-8 offset-2">
      {{-- error handling show all errors--}}
      @if ($errors->any())
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      {{-- call the Gate defined in AuthServiceProvider --}}
      {{-- @can('isAdmin') is same as using @if(Auth::user()->isAdmin == 1) --}}
      @can('isAdmin')
        {{-- admin dashboard view --}}
        <h2>Products</h2>
        <table class="table table-dark">
          <thead>
            <tr>
              <th>Name:</th>
              <th>Price:</th>
              <th>Status:</th>
              <th>Category:</th>
              <th>Actions:</th>
            </tr>
          </thead>

          <tbody>
            @foreach($products as $product)
					    <tr>
					      <td><a href="/products/{{$product->id}}">{{$product->name}}</a></td>
					      <td>{{$product->price}}</td>
					      <td>
                  @if($product->isActive == 1)
                    {{'Active'}}
                  @else
                    {{'Inactive'}}
                  @endif
                </td>
                {{-- relationship from Product model pointing to Category --}}
                {{-- note the product migration needs a foreign key category_id --}}
                {{-- joining the products to categories table to get the name in categories --}}
                <td>{{$product->category->name}}</td>
                {{-- action buttons --}}
                <td>
                  <a class="btn btn-warning mb-1" href="/products/{{$product->id}}/edit">Edit</a>
                  <form method="POST" action="/products/{{$product->id}}">
                    @csrf
                    @method('DELETE')
                    @if($product->isActive == 1)
                      <button type="submit" class="btn btn-danger">Deactivate</button>
                    @else
                      <button type="submit" class="btn btn-success">Reactivate</button>
                    @endif
                  </form>
                </td>
					    </tr>
						@endforeach
          </tbody>
        </table>
      @else
        {{-- check if a session flash variable containing a notification message is set --}}
        {{-- see global session helper --}}
        @if(session('status'))
          <div class="alert alert-primary" role="alert">
            {{session('status')}}
          </div>
        @endif
        <div class="row">
          {{-- catalogue view for non admin users --}}
          @foreach($products as $product)
            {{-- only display the active products in the catalogue --}}
            @if($product->isActive == 1)
              <div class="col-3 p-4">
                <div style="width:50%;" class="mx-auto">
                  <img src="{{asset($product->img_path)}}" class="card-img-top" width="100%">
                </div>
                <div class="card-body">
                  <h4 class="card-title"><a href="/products/{{$product->id}}">{{$product->name}}</a></h4>
                  <p class="card-text">{{$product->description}}</p>
                  <p class="card-text">{{$product->price}}</p>
                  {{-- in order to take advantage of session flash data we need to use a conventional HTML form --}}
                  <form action="/cart" method="POST">
                    @csrf
                    {{-- id is a prefilled form input that is hidden --}}
                    <input name="id" value="{{$product->id}}" hidden>
                    <input type="number" min="1" name="quantity" class="quantity">
                    <button type="submit" class="addToCart">Add to Cart</button>
                  </form>
                </div>
              </div>
            @endif
          @endforeach
        </div>
      @endcan
    </div>
  </div>
@endsection
