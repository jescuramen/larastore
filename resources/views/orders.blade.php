@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-8 offset-2">
      <h2>Transactions</h2>
      <table class="table table-dark">
        <thead>
          <th scope="col">Date:</th>
          <th scope="col">Total:</th>
          <th scope="col">Product Breakdown:</th>
        </thead>

        <tbody>
          @foreach($orders as $order)
            <tr>
              <td>{{$order->created_at}}</td>
              <td>{{$order->total}}</td>
              <td>
                <table class="table table-dark">
                  <thead>
                    <th scope="col">Product:</th>
                    <th scope="col">Quantity:</th>
                    <th scope="col">Subtotal:</th>
                  </thead>
                  <tbody>
                    @foreach($order->products as $product)
                      <tr>
                        <td>{{$product->name}}</td>
                        {{-- echo the products_orders pivot table's custom column quantity --}}
                        {{-- Eloquent ORM withPivot must be setup properly --}}
                        <td>{{$product->pivot->quantity}}</td>
                        <td>{{$product->pivot->subtotal}}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

@endsection
