@extends('layouts.app')

@section('content')
  <div class="row">
    <div class="col-8 offset-2">
      {{-- gate to not allow our admin to view the cart --}}
      @cannot('isAdmin')
        <h1>My Cart</h1>
        {{-- error handling show all errors--}}
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        {{-- check if a session flash variable containing a notification message is set --}}
        {{-- see global session helper --}}
        @if(session('status'))
          <div class="alert alert-primary" role="alert">
            {{session('status')}}
          </div>
        @endif
        {{-- we received a compact data $product_cart from CartController.php --}}
        @if($product_cart != null)
          <table class="table">
            <thead>
              <tr>
                <th scope="col">Product</th>
                <th scope="col">Quantity</th>
                <th scope="col">Price</th>
                <th scope="col">Subtotal</th>
                <th scope="col">Action</th>
              </tr>
            </thead>

            <tbody>
              @foreach($product_cart as $product)
                <tr>
                  <td>{{$product->name}}</td>
                  <td>
                    {{-- the $product->quantity will be inside a form --}}
                    <form action="/cart/{{$product->id}}" method="POST">
                      @csrf
                      {{-- form spoofing for action UPDATE is PUT --}}
                      @method('PUT')
                      <div class="row">
                        <div class="col-8">
                          {{-- quantity input --}}
                          <input type="number" name="newQty" value="{{$product->quantity}}" min="1" class="form-control">
                        </div>
                        <div class="col-4">
                          {{-- action button --}}
                          <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                      </div>
                    </form>
                  </td>
                  <td>{{$product->price}}</td>
                  <td>{{$product->subtotal}}</td>
                  <td>
                    <form class="" action="/cart/{{$product->id}}" method="POST">
                      @csrf
                      @method('DELETE')
                      <button class="btn btn-danger" type="submit">Delete</button>
                    </form>
                  </td>
                </tr>
              @endforeach
              {{-- after termination of @foreach loop add a total --}}
              {{-- create a row that will contain a total amount --}}
              <tr>
                <td></td>
                <td></td>
                <td></td>
                {{-- $total from CartController --}}
                <td>Total: <span id="total">{{$total}}</span></td>
                <td colspan="4">
                  @guest
                    <h5>Login or register to checkout.</h5>
                  @else
                    {{-- an HTML element container where our paypal smart buttons will be rendered in --}}
                    <div id="paypal"></div>
                  @endguest
                </td>
              </tr>
            </tbody>
          </table>
        @else
          {{-- just ouput a jumbotron if no cart session --}}
          <div class="jumbotron">
            <h1>Your cart is empty!</h1>
            <p class="lead">Why don't you go shopping first?</p>
            <a class="btn btn-primary btn-lg" href="/products">Catalogue</a>
          </div>
        @endif
      @else
        {{-- for admin who tried to view cart --}}
        <div class="jumbotron">
          <h1>You're not suppose to be here.</h1>
          <p class="lead">Why don't you just go back to the admin dashboard?</p>
          <a class="btn btn-primary btn-lg" href="/products">Dashboard</a>
        </div>
      @endcannot
    </div>
  </div>

  <script src="{{asset('js/paypal.js')}}"></script>
@endsection
