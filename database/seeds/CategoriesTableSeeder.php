<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'created_at' => '2020-04-07 12:33:58',
                'id' => 1,
                'name' => 'pets',
                'updated_at' => '2020-04-07 12:33:58',
            ),
            1 => 
            array (
                'created_at' => '2020-04-07 12:56:05',
                'id' => 2,
                'name' => 'accessories',
                'updated_at' => '2020-04-07 12:56:05',
            ),
            2 => 
            array (
                'created_at' => '2020-04-08 10:12:28',
                'id' => 3,
                'name' => 'miscellaneous',
                'updated_at' => '2020-04-08 10:12:28',
            ),
            3 => 
            array (
                'created_at' => '2020-04-08 10:13:47',
                'id' => 4,
                'name' => 'feeds',
                'updated_at' => '2020-04-08 10:13:47',
            ),
        ));
        
        
    }
}