<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'jes',
                'email' => 'jes@mail',
                'email_verified_at' => NULL,
                'password' => '$2y$10$6aLcsaUHbUMSn1cSbf.4Cui4nLv5GX/Gh0f3OjCckIBJFc115ACX2',
                'isAdmin' => 0,
                'remember_token' => NULL,
                'created_at' => '2020-04-06 10:08:18',
                'updated_at' => '2020-04-06 10:08:18',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'admin',
                'email' => 'admin@mail',
                'email_verified_at' => NULL,
                'password' => '$2y$10$brIpvQSaG/HjOb4bWli96Ottt.N2IBD8c9BkVIsU4DbW29ypOtMle',
                'isAdmin' => 1,
                'remember_token' => NULL,
                'created_at' => '2020-04-06 10:08:54',
                'updated_at' => '2020-04-06 10:08:54',
            ),
        ));
        
        
    }
}