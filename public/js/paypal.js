const paypalDiv = document.querySelector('#paypal');
const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
let total = document.querySelector('#total').innerHTML;

if(paypalDiv){
  paypal.Buttons({
    createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: total
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      // This function captures the funds from the transaction.
      return actions.order.capture().then(function(details) {
        // This function shows a transaction success message to your buyer.
        //alert(`Transaction completed by ${details.payer.name.given_name}`);
        //instantiate a new FormData object
        let data = new FormData();
        //append the total amount as an input with name 'total' to our FormData
        data.append('total', total);
        //if capturing of the order resolves
        //create a POST request to /orders endpoint via JS fetch in order to create a new order record.
        fetch('/orders', {
          method: "POST",
          //send the total via the body of this fetch request
          body: data,
          headers: {
            'X-CSRF-TOKEN': csrfToken
          }
        })
        .then(()=>{
          //redirect orders page
          window.location.replace('/orders');
        })
      });
    }
  }).render('#paypal');
  //This function displays Smart Payment Buttons on your web page.
}
